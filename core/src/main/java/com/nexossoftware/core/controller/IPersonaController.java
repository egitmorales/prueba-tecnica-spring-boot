package com.nexossoftware.core.controller;

import java.util.List;
import java.util.Optional;

import com.nexossoftware.core.entities.Persona;

/**
 * The Interface IPersonaController.
 */
public interface IPersonaController {
	
	/**
	 * Gets the personas.
	 *
	 * @return the personas
	 */
	public List<Persona> getPersonas();
	
	/**
	 * Gets the persona by id.
	 *
	 * @param id the id
	 * @return the persona by id
	 */
	public Optional<Persona> getPersonaById(Long id);
	
	/**
	 * Adds the persona.
	 *
	 * @param persona the persona
	 * @return the persona
	 */
	public Persona addPersona(Persona persona);
	
	/**
	 * Delete persona.
	 *
	 * @param id the id
	 * @return the string
	 */
	public String deletePersona (Long id);
	
	/**
	 * Update persona.
	 *
	 * @param persona the persona
	 * @return the string
	 */
	public String updatePersona (Persona persona);
	
}
