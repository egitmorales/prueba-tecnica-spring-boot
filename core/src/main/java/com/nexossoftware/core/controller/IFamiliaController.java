package com.nexossoftware.core.controller;

import java.util.List;
import java.util.Optional;

import com.nexossoftware.core.entities.Familia;

/**
 * The Interface IFamiliaController.
 */
public interface IFamiliaController {

	
	/**
	 * Gets the familias.
	 *
	 * @return the familias
	 */
	public List<Familia> getFamilias();
	
	/**
	 * Gets the familia by id.
	 *
	 * @param id the id
	 * @return the familia by id
	 */
	public Optional<Familia> getFamiliaById(Long id);
	
	/**
	 * Addfamilia.
	 *
	 * @param familia the familia
	 * @return the familia
	 */
	public Familia addfamilia(Familia familia);
	
	/**
	 * Delete familia.
	 *
	 * @param id the id
	 * @return the string
	 */
	public String deleteFamilia (Long id);
	
	/**
	 * Update familia.
	 *
	 * @param familia the familia
	 * @return the string
	 */
	public String updateFamilia (Familia familia);





}
