package com.nexossoftware.core.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The Class Familia.
 */
@Entity
@Table(name= "Familia")
public class Familia {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	Long id;
	
	/** The familiar 1. */
	@Column(name = "Familiar_1")
	Long familiar_1;
	
	/** The familiar 2. */
	@Column(name = "Familiar_2")
	Long familiar_2;
	
	/** The familiar 3. */
	@Column(name = "Familiar_3")
	Long familiar_3;
	
	/** The familiar 4. */
	@Column(name = "Familiar_4")
	Long familiar_4;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the familiar 1.
	 *
	 * @return the familiar 1
	 */
	public Long getFamiliar_1() {
		return familiar_1;
	}

	/**
	 * Sets the familiar 1.
	 *
	 * @param familiar_1 the new familiar 1
	 */
	public void setFamiliar_1(Long familiar_1) {
		this.familiar_1 = familiar_1;
	}

	/**
	 * Gets the familiar 2.
	 *
	 * @return the familiar 2
	 */
	public Long getFamiliar_2() {
		return familiar_2;
	}

	/**
	 * Sets the familiar 2.
	 *
	 * @param familiar_2 the new familiar 2
	 */
	public void setFamiliar_2(Long familiar_2) {
		this.familiar_2 = familiar_2;
	}

	/**
	 * Gets the familiar 3.
	 *
	 * @return the familiar 3
	 */
	public Long getFamiliar_3() {
		return familiar_3;
	}

	/**
	 * Sets the familiar 3.
	 *
	 * @param familiar_3 the new familiar 3
	 */
	public void setFamiliar_3(Long familiar_3) {
		this.familiar_3 = familiar_3;
	}

	/**
	 * Gets the familiar 4.
	 *
	 * @return the familiar 4
	 */
	public Long getFamiliar_4() {
		return familiar_4;
	}

	/**
	 * Sets the familiar 4.
	 *
	 * @param familiar_4 the new familiar 4
	 */
	public void setFamiliar_4(Long familiar_4) {
		this.familiar_4 = familiar_4;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Familia [id=" + id + ", familiar_1=" + familiar_1 + ", familiar_2=" + familiar_2 + ", familiar_3="
				+ familiar_3 + ", familiar_4=" + familiar_4 + "]";
	}
	
}
