package com.nexossoftware.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

/**
 * The Class Auditoria.
 */
@Entity
@Table(name= "Auditoria")
public class Auditoria {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	Long id;
	
	/** The entrada. */
	@Column(name = "Entrada")
	String entrada;
	
	/** The salida. */
	@Column(name = "Salida")
	String salida;
	
	/** The hora. */
	@Column(name = "Hora")
	Date hora;
	
	/** The servicio. */
	@Column(name = "Servicio")
	String servicio ;
	
	/**
	 * Pre persist.
	 */
	@PrePersist
	public void prePersist() {
		hora = new Date();
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * Gets the entrada.
	 *
	 * @return the entrada
	 */
	public String getEntrada() {
		return entrada;
	}

	/**
	 * Sets the entrada.
	 *
	 * @param entrada the new entrada
	 */
	public void setEntrada(String entrada) {
		this.entrada = entrada;
	}

	/**
	 * Gets the salida.
	 *
	 * @return the salida
	 */
	public String getSalida() {
		return salida;
	}

	/**
	 * Sets the salida.
	 *
	 * @param salida the new salida
	 */
	public void setSalida(String salida) {
		this.salida = salida;
	}

	/**
	 * Gets the hora.
	 *
	 * @return the hora
	 */
	public Date getHora() {
		return hora;
	}

	/**
	 * Sets the hora.
	 *
	 * @param hora the new hora
	 */
	public void setHora(Date hora) {
		this.hora = hora;
	}

	/**
	 * Gets the servicio.
	 *
	 * @return the servicio
	 */
	public String getServicio() {
		return servicio;
	}

	/**
	 * Sets the servicio.
	 *
	 * @param servicio the new servicio
	 */
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	
	
}
