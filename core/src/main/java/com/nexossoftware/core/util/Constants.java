package com.nexossoftware.core.util;

/**
 * The Class Constants.
 */
public class Constants {
	
	/**
	 * Instantiates a new constants.
	 */
	private Constants() {
		
	}
	
	/** The Constant ACTUALIZAR_EXITOSO. */
	public static final String ACTUALIZAR_EXITOSO = "Registro actualizado exitosamente.";
	
	/** The Constant ACTUALIZAR_FALLIDO. */
	public static final String ACTUALIZAR_FALLIDO = "No es posible actualizar el registro.";
	
	/** The Constant BORRAR_EXITOSO. */
	public static final String BORRAR_EXITOSO = "Registro eliminado exitosamente.";
	
	/** The Constant BORRAR_FALLIDO. */
	public static final String BORRAR_FALLIDO = "No es posible eliminar el registro.";
	
	/** The Constant LISTAR_PERSONAS. */
	public static final String LISTAR_PERSONAS = "/personas";
	
	/** The Constant BORRAR_PERSONA. */
	public static final String BORRAR_PERSONA = "/persona/delete/{id}";
	
	/** The Constant AGREGAR_PERSONA. */
	public static final String AGREGAR_PERSONA = "/persona/add";
	
	/** The Constant LISTAR_PERSONA. */
	public static final String LISTAR_PERSONA = "/persona/{id}";
	
	/** The Constant ACTUALIZAR_PERSONA. */
	public static final String ACTUALIZAR_PERSONA = "/persona/update";
	
	/** The Constant LISTAR_FAMILIAS. */
	public static final String LISTAR_FAMILIAS = "/familias";
	
	/** The Constant BORRAR_FAMILIA. */
	public static final String BORRAR_FAMILIA = "/familia/delete/{id}";
	
	/** The Constant AGREGAR_FAMILIA. */
	public static final String AGREGAR_FAMILIA = "/familia/add";
	
	/** The Constant LISTAR_FAMILIA. */
	public static final String LISTAR_FAMILIA = "/familia/{id}";
	
	/** The Constant ACTUALIZAR_FAMILIA. */
	public static final String ACTUALIZAR_FAMILIA = "/familia/update";
}
