package com.nexossoftware.core.service;

import java.util.List;
import java.util.Optional;

import com.nexossoftware.core.entities.Persona;

/**
 * The Interface IPersonaService.
 */
public interface IPersonaService {
	
	/**
	 * Find all personas.
	 *
	 * @return the list
	 */
	public List<Persona> findAllPersonas();
	
	/**
	 * Find persona by id.
	 *
	 * @param id the id
	 * @return the optional
	 */
	public Optional <Persona> findPersonaById(Long id);
	
	/**
	 * Save persona.
	 *
	 * @param personaNew the persona new
	 * @return the persona
	 */
	public Persona savePersona (Persona persona); 
	
	/**
	 * Delete persona.
	 *
	 * @param id the id
	 * @return the string
	 */
	public String deletePersona (Long id);	
	
	/**
	 * Update persona.
	 *
	 * @param personaNew the persona new
	 * @return the string
	 */
	public String updatePersona(Persona persona); 

}
