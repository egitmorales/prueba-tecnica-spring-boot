package com.nexossoftware.core.service;

import java.util.List;
import java.util.Optional;

import com.nexossoftware.core.entities.Familia;

/**
 * The Interface IFamiliaService.
 */
public interface IFamiliaService {
	
	/**
	 * Find all personas.
	 *
	 * @return the list
	 */
	public List<Familia> findAllFamilias();
	
	/**
	 * Findfamilia by id.
	 *
	 * @param id the id
	 * @return the optional
	 */
	public Optional <Familia> findfamiliaById(Long id);
	
	/**
	 * Save familia.
	 *
	 * @param familia the familia
	 * @return the familia
	 */
	public Familia saveFamilia (Familia familia); 
	
	/**
	 * Delete familia.
	 *
	 * @param id the id
	 * @return the string
	 */
	public String deleteFamilia (Long id);	
	
	/**
	 * Update familia.
	 *
	 * @param familia the familia
	 * @return the string
	 */
	public String updateFamilia(Familia familia); 






}
