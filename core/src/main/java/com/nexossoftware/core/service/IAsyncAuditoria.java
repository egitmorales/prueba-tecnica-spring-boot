package com.nexossoftware.core.service;


/**
 * The Interface IAsyncAuditoria.
 */
public interface IAsyncAuditoria {
	
	 /**
 	 * Save auditoria.
 	 *
 	 * @param auditoria the auditoria
 	 * @return the auditoria
 	 */
 	public void saveAuditoria (String servicio, String entrada, String salida);

 	
}
