package com.nexossoftware.core.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexossoftware.core.entities.Persona;

/**
 * The Interface PersonaRepository.
 */
public interface PersonaRepository extends JpaRepository<Persona, Long> {
	
	/**
	 * Save.
	 *
	 * @param personatoUpdate the persona to update
	 * @return the void
	 */
	Void save (Optional<Persona> personatoUpdate);

}
