package com.nexossoftware.core.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexossoftware.core.entities.Auditoria;

/**
 * The Interface AuditoriaRepository.
 */
public interface AuditoriaRepository extends JpaRepository<Auditoria, Long> {
	
	/**
	 * Save.
	 *
	 * @param AuditoriaToUpdate the auditoria to update
	 * @return the void
	 */
	Void save (Optional<Auditoria> AuditoriaToUpdate);


}
