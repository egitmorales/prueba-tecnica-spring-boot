package com.nexossoftware.core.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexossoftware.core.entities.Familia;

/**
 * The Interface FamiliaRepository.
 */
public interface FamiliaRepository  extends JpaRepository<Familia, Long>{
	
	/**
	 * Save.
	 *
	 * @param familiaToUpdate the familia to update
	 * @return the void
	 */
	Void save (Optional<Familia> familiaToUpdate);

}
