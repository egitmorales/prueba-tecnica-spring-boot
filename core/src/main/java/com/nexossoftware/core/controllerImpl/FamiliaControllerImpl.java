package com.nexossoftware.core.controllerImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.nexossoftware.core.controller.IFamiliaController;
import com.nexossoftware.core.entities.Familia;
import com.nexossoftware.core.serviceImpl.FamiliaServiceImpl;

/**
 * The Class FamiliaControllerImpl.
 */
@RestController
public class FamiliaControllerImpl implements IFamiliaController{

	/** The familia service. */
	@Autowired
	FamiliaServiceImpl familiaService;

	/**
	 * Gets the familias.
	 *
	 * @return the familias
	 */
	@Override
	@RequestMapping(value="/familias", method = RequestMethod.GET, produces = "application/json")
	public List<Familia> getFamilias() {
		try {
			return familiaService.findAllFamilias();
		} catch (Exception e) {
			throw new ResponseStatusException(
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Gets the familia by id.
	 *
	 * @param id the id
	 * @return the familia by id
	 */
	@Override
	@RequestMapping(value="/familia/{id}", method = RequestMethod.POST, produces = "application/json")
	public Optional<Familia> getFamiliaById(@PathVariable Long id) {
		try {
			return familiaService.findfamiliaById(id);
		} catch (Exception e) {
			throw new ResponseStatusException(
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Addfamilia.
	 *
	 * @param familia the familia
	 * @return the familia
	 */
	@Override
	@RequestMapping(value="/familia/add", method = RequestMethod.POST, produces = "application/json")
	public Familia addfamilia(Familia familia) {
		try {
			return familiaService.saveFamilia(familia);
		} catch (Exception e) {
			throw new ResponseStatusException(
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Delete familia.
	 *
	 * @param id the id
	 * @return the string
	 */
	@Override
	@RequestMapping(value="/familia/delete/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public String deleteFamilia(@PathVariable Long id) {
		try {
			return familiaService.deleteFamilia(id);
		} catch (Exception e) {
			throw new ResponseStatusException(
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Update familia.
	 *
	 * @param familia the familia
	 * @return the string
	 */
	@Override
	@RequestMapping(value="/familia/update", method = RequestMethod.PUT, produces = "application/json")
	public String updateFamilia(Familia familia) {
		try {
			return familiaService.updateFamilia(familia);
		} catch (Exception e) {
			throw new ResponseStatusException(
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
