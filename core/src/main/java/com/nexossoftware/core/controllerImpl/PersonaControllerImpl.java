package com.nexossoftware.core.controllerImpl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.nexossoftware.core.controller.IPersonaController;
import com.nexossoftware.core.entities.Persona;
import com.nexossoftware.core.serviceImpl.PersonaServiceImpl;

/**
 * The Class PersonaControllerImpl.
 */
@RestController	
public class PersonaControllerImpl implements IPersonaController{

	/** The persona service. */
	@Autowired
	PersonaServiceImpl personaService;

	/**
	 * Gets the personas.
	 *
	 * @return the personas
	 */
	@Override
	@RequestMapping(value="/personas", method = RequestMethod.GET, produces = "application/json")
	public List<Persona> getPersonas() {
		try {
			return personaService.findAllPersonas();
		} catch (Exception e) {
			throw new ResponseStatusException(
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Gets the persona by id.
	 *
	 * @param id the id
	 * @return the persona by id
	 */
	@Override
	@RequestMapping(value="/persona/{id}", method = RequestMethod.POST, produces = "application/json")
	public Optional<Persona> getPersonaById(@PathVariable Long id) {
		try {
			return personaService.findPersonaById(id);
		} catch (Exception e) {
			throw new ResponseStatusException(
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Adds the persona.
	 *
	 * @param persona the persona
	 * @return the persona
	 */
	@Override
	@RequestMapping(value="/persona/add", method = RequestMethod.POST, produces = "application/json")
	public Persona addPersona(Persona persona) {
		try {
			return personaService.savePersona(persona);
		} catch (Exception e) {
			throw new ResponseStatusException(
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Delete persona.
	 *
	 * @param id the id
	 * @return the string
	 */
	@Override
	@RequestMapping(value="/persona/delete/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public String deletePersona(@PathVariable Long id) {
		try {
			return personaService.deletePersona(id);
		} catch (Exception e) {
			throw new ResponseStatusException(
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Update persona.
	 *
	 * @param persona the persona
	 * @return the string
	 */
	@Override
	@RequestMapping(value="/persona/update", method = RequestMethod.PUT, produces = "application/json")
	public String updatePersona(Persona persona) {
		try {
			return personaService.updatePersona(persona);
		} catch (Exception e) {
			throw new ResponseStatusException(
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
