package com.nexossoftware.core.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nexossoftware.core.entities.Persona;
import com.nexossoftware.core.repository.PersonaRepository;
import com.nexossoftware.core.service.IPersonaService;
import com.nexossoftware.core.util.Constants;

/**
 * The Class PersonaServiceImpl.
 */
@Service
public class PersonaServiceImpl implements IPersonaService{

	/** The persona repository. */
	@Autowired
	PersonaRepository personaRepository;
	
	/** The auditoria service. */
	@Autowired
	AsyncAuditoriaImpl auditoriaService;

	/**
	 * Find all personas.
	 *
	 * @return the list
	 */
	@Override
	public List<Persona> findAllPersonas() {
		auditoriaService.saveAuditoria(Constants.LISTAR_PERSONAS, null, personaRepository.findAll().toString());
		return personaRepository.findAll();
	}

	/**
	 * Find persona by id.
	 *
	 * @param id the id
	 * @return the optional
	 */
	@Override
	public Optional<Persona> findPersonaById(Long id) {
		Optional<Persona> persona = personaRepository.findById(id);
		auditoriaService.saveAuditoria(Constants.LISTAR_PERSONA, id.toString(), persona.toString());
		return persona;
	}

	/**
	 * Save persona.
	 *
	 * @param persona the persona
	 * @return the persona
	 */
	@Override
	public Persona savePersona(Persona persona) {
		if(persona != null) {
			auditoriaService.saveAuditoria(Constants.AGREGAR_PERSONA, persona.toString(), persona.toString());
			return personaRepository.save(persona);
		}
		return new Persona();
	}

	/**
	 * Delete persona.
	 *
	 * @param id the id
	 * @return the string
	 */
	@Override
	public String deletePersona(Long id) {
		if(personaRepository.findById(id).isPresent()) {
			personaRepository.deleteById(id);
			auditoriaService.saveAuditoria(Constants.BORRAR_PERSONA, id.toString(), Constants.BORRAR_EXITOSO);
			return Constants.BORRAR_EXITOSO;
		}
		auditoriaService.saveAuditoria(Constants.BORRAR_PERSONA, id.toString(), Constants.BORRAR_FALLIDO);
		return Constants.BORRAR_FALLIDO;
	}

	/**
	 * Update persona.
	 *
	 * @param persona the persona
	 * @return the string
	 */
	@Override
	public String updatePersona(Persona persona) {

		Long num = persona.getId();
		
		if(personaRepository.findById(num).isPresent()) {
			Persona personaActualizada = new Persona();
			
			personaActualizada.setId(persona.getId());
			
			personaActualizada.setNombre(persona.getNombre());
			
			personaActualizada.setApellido(persona.getApellido());
			
			personaActualizada.setEdad(persona.getEdad());
			
			personaActualizada.setSexo(persona.getSexo());
			
			personaActualizada.setNit(persona.getNit());
			
			personaRepository.save(persona);
			
			auditoriaService.saveAuditoria(Constants.ACTUALIZAR_PERSONA, num.toString(), Constants.ACTUALIZAR_EXITOSO);
			return Constants.ACTUALIZAR_EXITOSO;
		}
		auditoriaService.saveAuditoria(Constants.ACTUALIZAR_PERSONA, num.toString(), Constants.ACTUALIZAR_FALLIDO);
		return Constants.ACTUALIZAR_FALLIDO;
	}

}
