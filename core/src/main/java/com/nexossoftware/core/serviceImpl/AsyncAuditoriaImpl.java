package com.nexossoftware.core.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.nexossoftware.core.entities.Auditoria;
import com.nexossoftware.core.repository.AuditoriaRepository;
import com.nexossoftware.core.service.IAsyncAuditoria;

/**
 * The Class AsyncAuditoriaImpl.
 */
@Service
public class AsyncAuditoriaImpl implements IAsyncAuditoria {
	
	/** The auditoria repository. */
	@Autowired
	AuditoriaRepository auditoriaRepository;

	/**
	 * Save auditoria.
	 *
	 * @param servicio the servicio
	 */
	@Async
	public void saveAuditoria(String servicio, String entrada, String salida) {
		
		Auditoria auditoria = new Auditoria();
		
		auditoria.setServicio(servicio);
		auditoria.setEntrada(entrada);
		auditoria.setSalida(salida);
		
		auditoriaRepository.save(auditoria);
		
	}

}
