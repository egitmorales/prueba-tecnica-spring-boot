package com.nexossoftware.core.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nexossoftware.core.entities.Familia;
import com.nexossoftware.core.repository.FamiliaRepository;
import com.nexossoftware.core.service.IFamiliaService;
import com.nexossoftware.core.util.Constants;

/**
 * The Class FamiliaServiceImpl.
 */
@Service
public class FamiliaServiceImpl implements  IFamiliaService{

	/** The familia repository. */
	@Autowired
	FamiliaRepository familiaRepository;

	/** The auditoria service. */
	@Autowired
	AsyncAuditoriaImpl auditoriaService;

	/**
	 * Find all familias.
	 *
	 * @return the list
	 */
	@Override
	public List<Familia> findAllFamilias() {
		auditoriaService.saveAuditoria(Constants.LISTAR_FAMILIAS, null, familiaRepository.findAll().toString());
		return familiaRepository.findAll();
	}

	/**
	 * Findfamilia by id.
	 *
	 * @param id the id
	 * @return the optional
	 */
	@Override
	public Optional<Familia> findfamiliaById(Long id) {
		Optional<Familia> familia = familiaRepository.findById(id);
		auditoriaService.saveAuditoria(Constants.LISTAR_FAMILIA, id.toString(), familia.toString());
		return familia;	
	}

	/**
	 * Save familia.
	 *
	 * @param familia the familia
	 * @return the familia
	 */
	@Override
	public Familia saveFamilia(Familia familia) {

		if(familia != null) {
			auditoriaService.saveAuditoria(Constants.AGREGAR_FAMILIA, familia.toString(), familia.toString());
			return familiaRepository.save(familia);	
		}
		auditoriaService.saveAuditoria(Constants.AGREGAR_FAMILIA, null, new Familia().toString());
		return new Familia();
	}

	/**
	 * Delete familia.
	 *
	 * @param id the id
	 * @return the string
	 */
	@Override
	public String deleteFamilia(Long id) {
		if(familiaRepository.findById(id).isPresent()) {
			familiaRepository.deleteById(id);
			auditoriaService.saveAuditoria(Constants.BORRAR_FAMILIA, id.toString(), Constants.BORRAR_EXITOSO);
			return Constants.BORRAR_EXITOSO;
		}
		auditoriaService.saveAuditoria(Constants.BORRAR_FAMILIA, id.toString(), Constants.BORRAR_FALLIDO);
		return Constants.BORRAR_FALLIDO;	
	}

	/**
	 * Update familia.
	 *
	 * @param familia the familia
	 * @return the string
	 */
	@Override
	public String updateFamilia(Familia familia) {
		Long num = familia.getId();
		if(familiaRepository.findById(num).isPresent()) {
			Familia familiaActualizada = new Familia();

			familiaActualizada.setId(familia.getId());
			familiaActualizada.setFamiliar_1(familia.getFamiliar_1());
			familiaActualizada.setFamiliar_2(familia.getFamiliar_2());
			familiaActualizada.setFamiliar_3(familia.getFamiliar_3());
			familiaActualizada.setFamiliar_4(familia.getFamiliar_4());
			familiaRepository.save(familiaActualizada);
			auditoriaService.saveAuditoria(Constants.ACTUALIZAR_FAMILIA, num.toString(), Constants.ACTUALIZAR_EXITOSO);
			return Constants.ACTUALIZAR_EXITOSO;
		}
		auditoriaService.saveAuditoria(Constants.ACTUALIZAR_FAMILIA, num.toString(), Constants.ACTUALIZAR_FALLIDO);
		return Constants.ACTUALIZAR_FALLIDO;
	}

}
