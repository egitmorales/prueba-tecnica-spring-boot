--Crea la base de datos

DROP DATABASE IF EXISTS familia;
CREATE DATABASE familia;
USE familia;

--Crea la tabla persona

CREATE TABLE Persona (
ID int NOT NULL AUTO_INCREMENT, 
Nombre varchar(20), 
Apellido varchar(20), 
Edad int, 
Sexo varchar(1), 
NIT varchar(10),
primary key (ID)); 

--Crea la tabla familia

Create Table Familia (
ID int NOT NULL AUTO_INCREMENT, 
Familiar_1 int,
Familiar_2 int, 
Familiar_3 int, 
Familiar_4 int,
primary key (ID));

--Crea la tabla auditoria

Create table auditoria(
ID int NOT NULL AUTO_INCREMENT, 
entrada blob,
salida blob,
Hora datetime, 
servicio varchar(20),
primary key (ID));


--Crea las llaves foraneas en familia

alter table familia 
add constraint fk_familiar1 
foreign key (familiar_1) references persona(ID);

alter table familia 
add constraint fk_familiar2
 foreign key (familiar_2) references persona(ID);

alter table familia
 add constraint fk_familiar3
  foreign key (familiar_3) references persona(ID);

alter table familia
 add constraint fk_familiar4
  foreign key (familiar_4) references persona(ID);

